# Import necessary libraries
import boto3
import os
from secret import * # Import your AWS credentials from a secure file

# Create an AWS session using your credentials
session = boto3.Session( aws_access_key_id, aws_secret_access_key, aws_session_token)

# Initialize an empty list to store S3 bucket names
list = []

# Create an S3 resource and an S3 client (resource is for s3 buckets and client is for presigned URL)
s3 = session.resource('s3')
s3url = session.client('s3')

# Initialize variables for S3 bucket name and file path
s3BucketName = ""
path = ""

# Define a function to list all S3 buckets
def listBuckets():
    global list
    list = []
    for i in s3.buckets.all():
        list.append(i.name)
    list.append("Create new S3 bucket")

# Check if the S3 bucket name and path are not provided
if s3BucketName and path == "":
    while True:
        # Define the main function for user interaction
        def main():
            global list
            listBuckets()
            print("Welcome, here is a list of all S3 buckets:", '\n',"\n"+"********************")
            counter = 1
            for i in list:
                print(f'{counter}:', i)
                counter += 1 
            print("********************", "\n")
            number = int(input("Please select an option: ")) 

            if number == len(list):
                bucketName = input("Please choose a unique name for your S3 Bucket: ")
                s3.create_bucket(Bucket=bucketName)

            else: 
                # Prompt the user to select a file path for uploading
                path = input(f"Please select a path for a file to upload to {list[number-1]}: ")
                path2 = os.path.basename(path)
                # Upload the file to the selected S3 bucket
                s3.Object(list[number-1], path2).put(Body=open(path, 'rb'))
                # Generate a presigned URL for the uploaded file
                url = s3url.generate_presigned_url('get_object', Params = {'Bucket': list[number-1], 'Key': path2}, ExpiresIn = 100)
                print('\n',f'Here is the public url for your file: {url}')
        print("")        
        main()
else:
    # If S3 bucket name and path are provided, skip user prompt and create the bucket and upload the file
    s3.create_bucket(Bucket=s3BucketName)
    s3.create_bucket(Bucket=s3BucketName)
    path2 = os.path.basename(path)
    s3.Object(s3BucketName, path2).put(Body=open(path, 'rb'))
    # Generate a presigned URL for the uploaded file
    url = s3url.generate_presigned_url('get_object', Params = {'Bucket': s3BucketName, 'Key': path2}, ExpiresIn = 100)
    print('\n'+f'Here is the public url for your file: {url}')
    print("")


