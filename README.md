I employed the following prompts:
 
        1:
                In Python, my variable is defined as follows:

                variable = "C:\\Users\\Skemi\\Desktop\\Github\\aws-deployment-challenge\\hello.txt"
                My objective is to extract the filename "hello.txt" from this variable. 

        2:
                Please add comments to my code that explains my code:

                {The code in script.py}

ChatGPT told me:

        1:

                Here is the code snippet I used to achieve this:

                import os

                filename = os.path.basename(variable)
                print(filename)

        2:

                Added the comments I was looking for and added them into script.py

I used the next sources for aid (In order):

        Setup a session with boto3: https://www.youtube.com/watch?v=9occfhrM4gg&list=LL&index=1
        How to List Contents of s3 Bucket Using Boto3 Python:  https://dev.to/aws-builders/how-to-list-contents-of-s3-bucket-using-boto3-python-47mm
        How to create a s3 bucket using Boto3: https://stackoverflow.com/questions/31092056/how-to-create-a-s3-bucket-using-boto3
        What are presigned urls: https://boto3.amazonaws.com/v1/documentation/api/latest/guide/s3-presigned-urls.html
        Presigned urls don't work with session.resource('s3') but do work with session.client('s3'): https://stackoverflow.com/questions/47469153/s3-object-has-no-attribute-bucket

For security, I used a seperate secret.py file to store my keys, script.py imports the keys from secret.py but .ignore skips the upload to Gitlab. The venv is also ignored.